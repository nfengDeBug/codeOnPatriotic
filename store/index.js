import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
		/*用户对象信息*/
		user:{},

	},
    mutations: {
	setUser(state,users){
		state.user=users; //参数是user 对象
		console.log(users)
	},

	},
	getters:{
		//获取用户信息 ，里面啥都有
		getUser(state){
			return state.user;
		},
	
	},
	actions:{
		getUser(content,users){
			return new Promise((resolve)=>{
				uni.request({
					url:'https://second.foxblog.xyz/user/add',
					method:'POST',
					data:{
						userName: users.username,
						img: users.avter,
						openId: users.openId,
					},
					success(res) {
					
						content.commit('setUser',res.data.user);
					},fail() {
						
					}
				})		
			})
			
		},
		
	}	
})

export default store