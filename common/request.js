const mapleBaseUrl = 'https://applet.nfeng.xyz/';
const dingdomBaseUrl = 'https://second.foxblog.xyz/';
export default {
	async get(data,name='maple', url = '') {
			return req(data, 'get',name, url)
		},

		async post(data,name='maple', url = '') {
			return req(data, 'post',name, url)
		}
}


function req(data, method,name,url) {
	return new Promise((resolve, reject) => {
		uni.request({
				url: (name === 'maple' ? mapleBaseUrl : dingdomBaseUrl) + url,
				data,
				method,
				header: {
					token: uni.getStorageSync("token") || ''
				},
				success(res){
						resolve(res.data)
					}
			})
	})
}

