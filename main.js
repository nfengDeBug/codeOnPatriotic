import App from './App'

// #ifndef VUE3
import uView from '@/uni_modules/uview-ui'//引用uView-ui
Vue.use(uView)//注册uView-ui
import Vue from 'vue'
Vue.config.productionTip = false
import store from './store'//引入vuex
App.mpType = 'app'
const app = new Vue({
		store,
    ...App
})
app.$mount()

// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif